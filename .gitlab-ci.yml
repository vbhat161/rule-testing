stages:
  - pull
  - consolidate
  - generate
  - compare
  - pushstats

.compare:
  stage: compare
  image: "registry.gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/rule-testing-framework/report-diff:1.7.1"

.generate_semgrep:
  stage: generate
  # use a temporary semgrep release that operates on go files
  image: "registry.gitlab.com/gitlab-org/security-products/analyzers/semgrep:latest"

##### BANDIT
pull_rules:
  stage: pull
  image: ruby:3-alpine
  before_script:
    - apk add git
  script:
    - ci/pull_rules.sh
  artifacts:
    paths:
      - "sast-rules/"

##### BANDIT
consolidate_bandit:
  stage: consolidate
  image: ruby:3-alpine
  needs:
    - job: pull_rules
      artifacts: true
  before_script:
    - apk add git
  script:
    - ci/consolidate.rb 
  artifacts:
    paths:
      - "bandit.yml"

bandit:
  stage: generate
  image: "registry.gitlab.com/gitlab-org/security-products/analyzers/bandit:2.11.1"
  needs:
    - job: consolidate_bandit
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - ci/generate_tests.sh "python" "bandit"
  script: 
    - (cd python-bandit-test && /analyzer run)
    - mv gl-sast-report.json bandit.json
  artifacts:
    paths:
      - "bandit.json"

semgrep_bandit:
  extends: .generate_semgrep
  needs:
    - job: consolidate_bandit
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - rm -rf /rules/*
    - cp bandit.yml /rules
    - ci/generate_tests.sh "python" "bandit"
  script: 
    - (cd python-bandit-test && /analyzer run)
    - mv gl-sast-report.json semgrep-bandit.json
  artifacts:
    paths:
      - "semgrep-bandit.json"

compare_bandit_semgrep:
  extends: .compare
  variables:
    DIFF_BASELINE: "bandit.json"
    DIFF_TESTFILE: "semgrep-bandit.json"
    DIFF_MAPPING: "sast-rules/mappings/bandit.yml"
    MARKDOWN_OUT: "bandit_semgrep.md"
    RULE_REPO: "https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules"
    JSON_OUT: "bandit_semgrep.json"
  needs:
    - job: bandit
      artifacts: true
    - job: semgrep_bandit
      artifacts: true
    - job: pull_rules
      artifacts: true
  script: 
    - /exe/diff
  artifacts:
    paths:
      - "bandit_semgrep.md"
      - "bandit_semgrep.json"

##### ESLINT
consolidate_eslint:
  stage: consolidate
  image: ruby:3-alpine
  needs:
    - job: pull_rules
      artifacts: true
  script:
    - ci/consolidate.rb
  artifacts:
    paths:
      - "eslint.yml"

eslint:
  stage: generate
  image: "registry.gitlab.com/gitlab-org/security-products/analyzers/eslint:2.20.0"
  needs:
    - job: consolidate_eslint
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - ci/generate_tests.sh "javascript" "eslint"
  script: 
    - (cd javascript-eslint-test && /analyzer run)
    - mv gl-sast-report.json eslint.json
  artifacts:
    paths:
      - "eslint.json"

semgrep_eslint:
  extends: .generate_semgrep
  needs:
    - job: consolidate_eslint
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - apk add git
    - rm -rf /rules/*
    - cp eslint.yml /rules
    - ci/generate_tests.sh "javascript" "eslint"
  script: 
    - (cd javascript-eslint-test && /analyzer run)
    - mv gl-sast-report.json semgrep-eslint.json
  artifacts:
    paths:
      - "semgrep-eslint.json"

compare_eslint_semgrep:
  extends: .compare
  variables:
    DIFF_BASELINE: "eslint.json"
    DIFF_TESTFILE: "semgrep-eslint.json"
    DIFF_MAPPING: "sast-rules/mappings/eslint.yml"
    MARKDOWN_OUT: "eslint_semgrep.md"
    JSON_OUT: "eslint_semgrep.json"
    RULE_REPO: "https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules"
  needs:
    - job: eslint
      artifacts: true
    - job: semgrep_eslint
      artifacts: true
    - job: pull_rules
      artifacts: true
  script: 
    - /exe/diff
  artifacts:
    paths:
      - "eslint_semgrep.md"
      - "eslint_semgrep.json"

##### FLAWFINDER
consolidate_flawfinder:
  stage: consolidate
  image: ruby:3-alpine
  needs:
    - job: pull_rules
      artifacts: true
  script:
    - ci/consolidate.rb
  artifacts:
    paths:
      - "flawfinder.yml"

flawfinder:
  stage: generate
  variables: 
    ANALYZER_OPTIMIZE_REPORT: 0
  image: "registry.gitlab.com/gitlab-org/security-products/analyzers/flawfinder:latest"
  needs:
    - job: consolidate_flawfinder
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - ci/generate_tests.sh "c" "flawfinder"
  script: 
    - (cd c-flawfinder-test && /analyzer run)
    - mv gl-sast-report.json flawfinder.json
  artifacts:
    paths:
      - "flawfinder.json"

semgrep_flawfinder:
  stage: generate
  image: "registry.gitlab.com/gitlab-org/security-products/analyzers/semgrep/tmp:2f09294b8ee697d16a6a1550b4e22de9554f2788"
  needs:
    - job: consolidate_flawfinder
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - apk add git
    - rm -rf /rules/*
    - cp flawfinder.yml /rules
    - ci/generate_tests.sh "c" "flawfinder"
  script: 
    - (cd c-flawfinder-test && /analyzer run)
    - mv gl-sast-report.json semgrep-flawfinder.json
  artifacts:
    paths:
      - "semgrep-flawfinder.json"

compare_flawfinder_semgrep:
  extends: .compare
  variables:
    DIFF_BASELINE: "flawfinder.json"
    DIFF_TESTFILE: "semgrep-flawfinder.json"
    DIFF_MAPPING: "sast-rules/mappings/flawfinder.yml"
    MARKDOWN_OUT: "flawfinder_semgrep.md"
    JSON_OUT: "flawfinder_semgrep.json"
    RULE_REPO: "https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules"
  needs:
    - job: flawfinder
      artifacts: true
    - job: semgrep_flawfinder
      artifacts: true
    - job: pull_rules
      artifacts: true
  script: 
    - /exe/diff
  artifacts:
    paths:
      - "flawfinder_semgrep.md"
      - "flawfinder_semgrep.json"

## GOSEC
consolidate_gosec:
  stage: consolidate
  image: ruby:3-alpine
  needs:
    - job: pull_rules
      artifacts: true
  script:
    - ci/consolidate.rb
  artifacts:
    paths:
      - "gosec.yml"

gosec:
  stage: generate
  image: "registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:3.2.1"
  needs:
    - job: consolidate_gosec
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - ci/generate_tests.sh "go" "gosec"
  script: 
    - (cd go-gosec-test && /analyzer run)
    - mv gl-sast-report.json gosec.json
  artifacts:
    paths:
      - "gosec.json"

semgrep_gosec:
  extends: .generate_semgrep
  variables:
    SECURE_LOG_LEVEL: "debug"
  needs:
    - job: consolidate_gosec
      artifacts: true
    - job: pull_rules
      artifacts: true
  before_script:
    - rm -rf /rules/*
    - cp gosec.yml /rules
    - ci/generate_tests.sh "go" "gosec"
  script: 
    - (cd go-gosec-test && /analyzer run)
    - mv gl-sast-report.json semgrep-gosec.json
  artifacts:
    paths:
      - "semgrep-gosec.json"

compare_gosec_semgrep:
  extends: .compare
  variables:
    DIFF_BASELINE: "gosec.json"
    DIFF_TESTFILE: "semgrep-gosec.json"
    DIFF_MAPPING: "sast-rules/mappings/gosec.yml"
    MARKDOWN_OUT: "gosec_semgrep.md"
    RULE_REPO: "https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules"
    JSON_OUT: "gosec_semgrep.json"
  needs:
    - job: gosec
      artifacts: true
    - job: semgrep_gosec
      artifacts: true
    - job: pull_rules
      artifacts: true
  script: 
    - /exe/diff
  artifacts:
    paths:
      - "gosec_semgrep.md"
      - "gosec_semgrep.json"

pushstats:
  stage: pushstats
  rules:
    - if: '$UPSTREAM_BRANCH == $CI_DEFAULT_BRANCH'
  needs:
    - job: compare_bandit_semgrep
      artifacts: true
    - job: compare_eslint_semgrep
      artifacts: true
    - job: compare_flawfinder_semgrep
      artifacts: true
    - job: compare_gosec_semgrep
      artifacts: true
  before_script:
    - ci/prep_stats_repo.sh
  script: 
    - mv "bandit_semgrep.md" "rule-testing-stats/"
    - mv "bandit_semgrep.json" "rule-testing-stats/"
    - mv "eslint_semgrep.md" "rule-testing-stats/"
    - mv "eslint_semgrep.json" "rule-testing-stats/"
    - mv "flawfinder_semgrep.md" "rule-testing-stats/"
    - mv "flawfinder_semgrep.json" "rule-testing-stats/"
    - mv "gosec_semgrep.md" "rule-testing-stats/"
    - mv "gosec_semgrep.json" "rule-testing-stats/"
    - (cd rule-testing-stats && git add --all && git commit -m "stats update" && git push || true)
