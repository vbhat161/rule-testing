# GitLab SAST rule testing framework

The GitLab SAST rule testing framework enables us to effectively test the
GitLab SAST rules contained in the central [SAST rule repository](https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules).

The structure of the rule testing framework is depicted below using brakeman
and flawfinder as examples.

``` mermaid
flowchart LR

crr[GitLab Rule Repository]

bandit(GitLab bandit)
bx[gl-sast-report.json]
sbx[gl-sast-report.json]
breport[bandit gap analysis report]

subgraph bandit comparison
   banditsemgrep(GitLab semgrep)
   banditcompare(compare)
   bandit --> |run analyzer on test-cases| bx;
   banditsemgrep --> |run analyzer on test-cases| sbx;
   bx --> banditcompare
   sbx --> banditcompare
end
crr -->|bandit rules + rule id mappings| banditsemgrep;
banditcompare --> breport

fx[gl-sast-report.json]
fbx[gl-sast-report.json]
freport[flawfinder gap analysis report]
flawfinder(GitLab flawfinder)

subgraph flawfinder comparison
   flawfindersemgrep(GitLab semgrep)
   flawfindercompare(compare)
   flawfinder --> |run analyzer on test-cases| fx;
   flawfindersemgrep --> |run analyzer on test-cases| fbx;
   fx --> flawfindercompare
   fbx --> flawfindercompare
end
crr -->|flawfinder rules + rule id mappings| flawfindersemgrep;
flawfindercompare --> freport
```

The baseline for the gap analysis is
provided by the original analyzer when executed on the test-files. The rules in
the central rule repository are compared and evaluated against this baseline.
In the example above, we use [GitLab bandit](https://gitlab.com/gitlab-org/security-products/analyzers/bandit),
[GitLab flawfinder](https://gitlab.com/gitlab-org/security-products/analyzers/flawfinder)
as examples. We rely on the GitLab analyzers because they are
already dockerized and produce a common `gl-sast-report.json` which makes
cross-validation fairly straightforward.

