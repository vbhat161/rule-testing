#!/bin/sh

git clone "https://guest:${RULE_TESTING_STATS_TOKEN}@gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/rule-testing-framework/rule-testing-stats"
(cd rule-testing-stats && git config user.name "GitLab Bot" && git config user.email "gitlab-bot@gitlab.com")

exit 0

