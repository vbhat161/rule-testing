#!/bin/sh

LANG="$1"
NAME="$2"

# config
CUR="$PWD"

TEST_DIR="$CUR/${LANG}-${NAME}-test"

[ -d "$TEST_DIR" ] || mkdir "$TEST_DIR"

trap "cd $CUR" EXIT

cd "sast-rules/${LANG}"

# flatten file structure
find * -name "test-*" | while read -r testfile; do
    target=$(echo "$testfile" | tr '/' '#')
    cp "$testfile" "$TEST_DIR/$target"
done

cd "$CUR"

exit 0
