#!/usr/bin/env ruby
# This script consolidates all yaml files to a single rule files

require 'yaml'
require_relative './autoformat.rb'

Dir.glob('sast-rules/mappings/*.yml').each do |mappings|
  prefix = File.basename(mappings, ".yml")
  dict = YAML.safe_load(File.read(mappings))

  rulez = {}
  id2rules = {}
  rule2ids = {}
  ruleids = {}
  rulefiles = []

  dict[prefix].each do |mapping|
    id = mapping['id']
    ruleids[id] = ruleids.keys.size unless ruleids.key?(id)
    id2rules[id] = [] unless id2rules.key?(id)
    mapping['rules'].each do |rule|
      rule2ids[rule] = [] unless rule2ids.key?(rule)
      rule2ids[rule] << id
      id2rules[id] << rule
      rulefiles << rule
    end
  end
  
  rulefiles.sort!.uniq!

  rulefiles.each do |rfil|
    target = File.join("sast-rules", "#{rfil}.yml")
    rulefiledict = YAML.safe_load(File.read(target))
    rulefromfile = rulefiledict['rules'].first
    rulez[rulefromfile['id']] = rulefromfile
  end

  outdict = { 'rules' => rulez.values }

  formatted = AutoFormat.reformat_yaml('', outdict)
  puts("writing #{prefix}.yml")
  File.open("#{prefix}.yml", 'w') { |file| 
    file.write(formatted) 
  }
end
