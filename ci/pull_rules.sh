#!/bin/sh

RULE_REPO="https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules"

echo "checking out $UPSTREAM_BRANCH"

# if UPSTREAM_BRANCH is not set, we are considering the main branch
[ -z "$UPSTREAM_BRANCH" ] && { 
    git clone "$RULE_REPO" --depth 1 || exit 1
}

# UPSTREAM_BRANCH is set by the upstream branch and specifies which branch we are
# pulling
[ -n "$UPSTREAM_BRANCH" ] && { 
    echo "$UPSTREAM_BRANCH" | grep -E "[a-zA-Z0-9_-]+" || exit 1 
    git clone "$RULE_REPO" || exit 1
    (cd sast-rules && git checkout "$UPSTREAM_BRANCH") 
}

exit 0
